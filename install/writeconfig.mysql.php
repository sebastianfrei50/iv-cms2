<?php

if(
	!empty( $_POST['sql_host'] ) &&
	!empty( $_POST['sql_user'] ) &&
	isset( $_POST['sql_pass'] ) &&
	!empty( $_POST['sql_db'] )
) {
	try {
		if( $_POST['use_ftp'] ) {
			$writer = new writer_ftp( $_POST['ftp_host'], $_POST['ftp_user'], $_POST['ftp_pass'], $_POST['ftp_dir'] );
		} else {
			$writer = new writer_fs();
		}

		// Test connections
		$db = new mysql_connection( $_POST['sql_host'], $_POST['sql_user'], $_POST['sql_pass'], $_POST['sql_db'] );

		// Install basic database
		initDb( $db );

		// Write Config files
		if( $_POST['use_ftp'] ) {
			$writer->put( 'inc/ftp.config.php', sprintf(
						"<?php\n\n\$ftp_host = '%s';\n\$ftp_user = '%s';\n\$ftp_pass = '%s';\n\$ftp_dir = '%s';\n",
						$_POST['ftp_host'], $_POST['ftp_user'], $_POST['ftp_pass'], $_POST['ftp_dir'] ));
		}

		$writer->put( 'inc/database.config.php', sprintf(
						"<?php\n\n\$db = new mysql_connection( '%s', '%s', '%s', '%s');\n\$db->set_charset('utf8');\n",
						$_POST['sql_host'], $_POST['sql_user'], $_POST['sql_pass'], $_POST['sql_db'] ));

		$db->update_server->insert($server);

		// Redirect
		header( 'LOCATION: '.IV_SELF );
		exit();
	} catch( Exception $e ) {
		$error = $e->getMessage();
	}
}
