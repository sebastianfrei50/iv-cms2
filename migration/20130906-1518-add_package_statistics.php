<?php

function install() {
	db()->query("ALTER TABLE `update_package`
		ADD `installations` INT UNSIGNED NOT NULL AFTER `version` ,
		ADD `updates` INT UNSIGNED NOT NULL AFTER `installations` ;");
}

function remove() {
	db()->query("ALTER TABLE `update_package`
		DROP COLUMN `installations`,
		DROP COLUMN  `updates`;");
}
