<?php

class widget_box {
	protected $title;
	protected $content;
	protected $width;

	public function __construct( $content, $title = "Content Box", $width = NULL ) {
		$this->title = $title;
		$this->content = $content;
		$this->width = $width;
	}

	public function __toString() {
		return template('box')->render( array(
				'title' => $this->title,
				'content' => $this->content,
				'width' => $this->width ));
	}
}
