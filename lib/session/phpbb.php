<?php

class session_phpbb {
	protected $name;
	protected $dbPre;
	protected $cookie_lifetime = 604800;

	/**
	 * Constructor starts a Session
	 * @param string $name Session Name
	 */
	public function __construct( $phpbbDB, $phpbbPrefix = 'phpbb_' ) {
		$this->dbPre = $phpbbDB.'.'.$phpbbPrefix;
		$name = db()->query("SELECT config_value FROM {$this->dbPre}config WHERE config_name = 'cookie_name'")->value();

		session_name( $this->name = $name );
		session_start();
	}
	/**
	 * Deletes relogin Cookies
	 * @return boolean false
	 */
	protected function killCookie() {
		$lifetime = time() -1;
		setcookie( $this->name."_k", 0, $lifetime );
		setcookie( $this->name."_u", 0, $lifetime );
		setcookie( $this->name."_sid", 0, $lifetime );
		return false;
	}

	/**
	 * Terminates a Session
	 */
	public function logout() {
		session_unset();
		$this->killCookie();
	}

	/**
	 * Checks if user ist logged in and returns user Obejct otherwise false
	 * @param int $flag
	 * @return mixed
	 */
	public function user( $flag = 0 ) {
		$uid = $_COOKIE[$this->name.'_u'];
		$sid = $_COOKIE[$this->name.'_sid'];

		$session = db()->query("SELECT *
			FROM {$this->dbPre}sessions s
			JOIN {$this->dbPre}users u ON (s.session_user_id = u.user_id)
			WHERE session_id = '%s' AND session_user_id = %d", $sid, $uid)->assoc();

		if( !$session ) return false;
		if( $session['user_type'] == 2 ) return false;
		if( $session['session_ip'] != $_SERVER['REMOTE_ADDR'] ) return false;

		if( !$user = db()->user_data->id( $uid )) {
			db()->user_data->insert(array(
				'id' => $session['user_id'],
				'type' => 1,
				'name' => $session['username'],
				'email' => $session['user_email'],
				'pass_hash' => $session['user_password']
			));

			$user = db()->user_data->id( $uid );
		}

		if( !$user ) return false;
		if( $user->type & $flag != $flag ) return false;

		db()->user_data->updateRow( array(
			'last_refresh' => time(),
			'last_ip' => $_SERVER['REMOTE_ADDR']
		), $user->id );

		$user->pns = $session['user_unread_privmsg'];

		return $user;
	}

	public function flag( $flag ) {
		$this->flags = $this->flags ^ $flag;
	}

	/**
	 * Preformes relogin based on cookie data
	 * @param int $user
	 * @param string $key
	 * @return boolean
	 */
	public function relogin( $flag = 0 ) {
		return false;
	}


	/**
	 * Performes login
	 * @param string $name
	 * @param string $pass
	 * @param boolean $relogin
	 * @param int $flag
	 * @return boolean
	 */
	public function login( $name, $pass, $relogin = false, $flag = 0 ) {
		return false;
	}

	/**
	 * This method is evil and should only avoid copy & paste
	 */
	public function changePassword( $action ) {
		return false;
	}

	/**
	 * Reads a session variable
	 * @param string $varname
	 * @return mixed
	 */
	public function __get($varname) {
		return $_SESSION[$varname];
	}

	/**
	 * Set as session variable
	 * @param string $varname
	 * @param mixed $value
	 */
	public function __set($varname, $value) {
		$_SESSION[$varname] = $value;
	}

	/**
	 * Erzeugt einen login key und transformiert diesen in den passwiord hash
	 * @param string $string
	 * @param string $salt
	 * @param int $type
	 * @return string
	 */
	public static function crypt( $string, $salt, $type = 0 ) {
		return self::transform( self::loginKey( $string, $salt), $salt );
	}

	/**
	 * transformirt den login key in den passwort hash
	 * @param string $string
	 * @param string $salt
	 * @return string
	 */
	protected static function transform( $string, $salt ) {
		return md5( $salt.$string );
	}

	/**
	 * erzeugt einen login key
	 * @param string $string
	 * @param string $salt
	 * @return string
	 */
	protected static function loginKey( $string, $salt ) {
		return md5( $string.$salt );
	}
}
