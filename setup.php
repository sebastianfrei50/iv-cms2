<?php

if(PHP_SAPI != 'cli') die('Please run this script in cli mode only');

// Autoloading für Klassen
function __autoload( $class ) {
	$file = 'lib/' . str_replace( '_', '/', $class ) . '.php';
	if( file_exists( $file )) require( $file );
}

function db() {
	return $GLOBALS['db'];
}

// Funktion
require 'inc/functions.php';
// Datenbankverbindung herstellen
require 'inc/database.config.php';

if( empty( $argv[1] )) {
	require 'install/init.mysql.php';
	initDb($db);

	foreach( update_migration::listPending() as $migration ) {
		if( !system('php setup.php '.$migration )) {
			break;
		}
	}
} else {
	$migrations = new update_migration();
	$migrations->install( $argv[1] );
	echo "\t {$argv[1]} installed.\n";
}

